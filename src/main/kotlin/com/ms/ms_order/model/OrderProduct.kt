package com.ms.ms_order.model

import com.ms.ms_order.model.base.BaseModel
import com.ms.ms_order.model.listener.OrderListener
import com.ms.ms_order.model.listener.OrderProductListener
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.util.*
import javax.persistence.*

@Entity
@EntityListeners(
    value = [
        OrderProductListener::class,
        AuditingEntityListener::class,
    ]
)

@Table(name = "order_product")
data class OrderProduct(
    @ManyToOne(fetch = FetchType.LAZY)
    var order: Order? = null,
    var subTotal: Double? = null,
    var total: Double? = null,
    var totalTax: Double? = null,
    var productUuid: UUID? = null,
    var quantity: Int? = null
) : BaseModel()