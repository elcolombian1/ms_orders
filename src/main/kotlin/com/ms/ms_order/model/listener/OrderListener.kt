package com.ms.ms_order.model.listener

import com.ms.ms_order.kafka.producer.OrderProducer
import com.ms.ms_order.model.Order
import com.ms.ms_order.model.base.CodeSetter
import com.ms.ms_order.repository.orderRepository.OrderRepository
import org.springframework.stereotype.Component
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import javax.persistence.PostPersist
import javax.persistence.PrePersist

@Component
class OrderListener : CodeSetter() {
    companion object {
        lateinit var orderRepository: OrderRepository
    }

    @Autowired
    fun setCompanion(
        _orderRepository: OrderRepository,
    ) {
        orderRepository = _orderRepository
    }

    private val log = LoggerFactory.getLogger(javaClass)

    @PrePersist
    fun prePersist(order: Order) {
        this.setTulCode(orderRepository, order)
    }
}