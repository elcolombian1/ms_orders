package com.ms.ms_order.model.listener

import com.ms.ms_order.kafka.producer.OrderProducer
import com.ms.ms_order.model.Order
import com.ms.ms_order.model.OrderProduct
import com.ms.ms_order.model.base.CodeSetter
import com.ms.ms_order.repository.orderProductRepository.OrderProductRepository
import org.springframework.stereotype.Component
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import javax.persistence.PostPersist
import javax.persistence.PrePersist

@Component
class OrderProductListener : CodeSetter() {
    companion object {
        lateinit var orderProductRepository: OrderProductRepository
    }

    @Autowired
    fun setCompanion(
        _orderProductRepository: OrderProductRepository,
    ) {
        orderProductRepository = _orderProductRepository
    }

    private val log = LoggerFactory.getLogger(javaClass)

    @PrePersist
    fun prePersist(orderProduct: OrderProduct) {
        this.setTulCode(orderProductRepository, orderProduct)
    }


}