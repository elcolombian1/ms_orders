package com.ms.ms_order.model.base

import com.ms.ms_order.repository.baseRepository.BaseRepository

abstract class CodeSetter {
    fun setTulCode(repository: BaseRepository, baseModel: BaseModel, prefix: String? = null) {
        if (baseModel.tulCode == null) {
            val maxTulCode =
                repository.findFirstByTulCodeNotNullOrderByCreatedAtDesc()?.tulCode?.substring(3)?.toLong() ?: 0
            baseModel.tulCode = baseModel.getCode(maxTulCode + 1, prefix)
        }
    }
}