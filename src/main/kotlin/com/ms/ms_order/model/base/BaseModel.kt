package com.ms.ms_order.model.base;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime;
import java.util.UUID;
import javax.persistence.*


@MappedSuperclass
@EntityListeners(
    value = [
        AuditingEntityListener::class
    ]
)

abstract class BaseModel {

    @Id
    var uuid: UUID? = UUID.randomUUID()

    @Column(unique = true)
    var tulCode: String? = null

    @Version
    var version: Long? = null

    @Column(name = "created_at", updatable = false)
    @CreatedDate
    var createdAt: LocalDateTime? = LocalDateTime.now()

    @Column(name = "last_modified_at")
    @LastModifiedDate
    var lastModifiedAt: LocalDateTime? = null


    fun getCode(counter: Long, prefix: String? = null): String {
        return (prefix ?: this.javaClass.simpleName.substring(0..1)
            .uppercase()) + "-" + String.format(String.format("%%0%dd", 6), counter)
    }
}