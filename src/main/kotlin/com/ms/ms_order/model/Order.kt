package com.ms.ms_order.model

import com.ms.ms_order.model.base.BaseModel
import com.ms.ms_order.model.listener.OrderListener
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.util.*
import javax.persistence.*


@Entity
@EntityListeners(
    value = [
        OrderListener::class,
        AuditingEntityListener::class,
    ]
)

@Table(name = "orders")
data class Order(
    var clientUuid: UUID,
    var subTotal: Double,
    var total: Double,
    var totalTax: Double,
    var status: String,
    var pickingStatus:String?="pending",
    var dispatchStatus:String?="pending",
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order")
    var products: List<OrderProduct>? = null
) : BaseModel()