package com.ms.ms_order.model.enums

enum class LogisticStatusEnum {
    pending,
    completed,
    cancelled
}