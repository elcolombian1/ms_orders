package com.ms.ms_order.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.ms.ms_order.kafka.factory.KafkaMessage
import com.ms.ms_order.config.KafkaConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory
import org.springframework.kafka.core.DefaultKafkaConsumerFactory
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer
import org.springframework.kafka.support.serializer.JsonDeserializer

@Configuration
class KafkaConsumerConfig(private val objectMapper: ObjectMapper) {

    @Bean(name = ["config.kafka.consumerFactory"])
    fun consumerFactory(kafkaProperties: KafkaProperties): ConcurrentKafkaListenerContainerFactory<String, KafkaMessage> {
        val containerFactory: ConcurrentKafkaListenerContainerFactory<String, KafkaMessage> =
            ConcurrentKafkaListenerContainerFactory<String, KafkaMessage>()
        val errorHandlingDeserializer =
            ErrorHandlingDeserializer(JsonDeserializer(KafkaMessage::class.java, objectMapper))
        val consumerFactory: DefaultKafkaConsumerFactory<String, KafkaMessage> = DefaultKafkaConsumerFactory(
            KafkaConfig.getConfig(kafkaProperties),
            StringDeserializer(),
            errorHandlingDeserializer
        )
        containerFactory.consumerFactory = consumerFactory
        return containerFactory
    }
}
