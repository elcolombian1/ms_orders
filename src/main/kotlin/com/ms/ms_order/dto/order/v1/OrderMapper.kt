package com.ms.ms_order.dto.order.v1

import com.ms.ms_order.model.Order
import org.mapstruct.*

@Mapper(
    componentModel = "spring",
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
)
interface OrderMapper {
    @Mapping(target = "orderProducts", source = "order.products")
    fun modelToDto(order: Order): OrderDto
    fun requestToModel(order: OrderRequest): Order
    fun update(request: OrderRequest, @MappingTarget model: Order)

}