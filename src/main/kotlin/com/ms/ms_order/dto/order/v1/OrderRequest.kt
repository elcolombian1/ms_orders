package com.ms.ms_order.dto.order.v1

import com.fasterxml.jackson.annotation.JsonProperty
import com.ms.ms_order.dto.orderProduct.v1.OrderProductRequest
import java.util.UUID

class OrderRequest {
    @JsonProperty("client_uuid")
    var clientUuid: UUID? = null

    @JsonProperty("sub_total")
    var subTotal: Double? = null
    var total: Double? = null

    @JsonProperty("total_tax")
    var totalTax: Double? = null
    var status: String? = null

    @JsonProperty("order_products")
    var orderProducts: List<OrderProductRequest>? = null
}