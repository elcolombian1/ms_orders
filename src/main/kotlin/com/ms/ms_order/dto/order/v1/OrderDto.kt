package com.ms.ms_order.dto.order.v1

import com.ms.ms_order.dto.BaseDto
import com.ms.ms_order.dto.orderProduct.v1.OrderProductDto
import com.ms.ms_order.model.OrderProduct
import java.util.UUID

class OrderDto : BaseDto() {
    var clientUuid: UUID? = null
    var subTotal: Double? = null
    var total: Double? = null
    var totalTax: Double? = null
    var status: String? = null
    var pickingStatus:String?=null
    var dispatchStatus:String?=null
    var orderProducts: List<OrderProductDto>? = null
}