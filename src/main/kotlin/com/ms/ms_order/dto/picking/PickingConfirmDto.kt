package com.ms.ms_order.dto.picking

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.UUID

class PickingConfirmDto {
    @JsonProperty("order_uuid")
    var orderUuid: UUID?=null
}