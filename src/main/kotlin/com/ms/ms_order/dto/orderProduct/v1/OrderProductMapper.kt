package com.ms.ms_order.dto.orderProduct.v1

import com.ms.ms_order.model.Order
import com.ms.ms_order.model.OrderProduct
import org.mapstruct.*

@Mapper(
    componentModel = "spring",
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
)
interface OrderProductMapper {
    fun requestToModel(orderProduct: OrderProductRequest): OrderProduct
    fun modelToDto(orderProduct: OrderProduct): OrderProductDto

}