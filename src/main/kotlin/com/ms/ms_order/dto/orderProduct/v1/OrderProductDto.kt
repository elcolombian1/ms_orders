package com.ms.ms_order.dto.orderProduct.v1

import com.fasterxml.jackson.annotation.JsonProperty
import com.ms.ms_order.model.Order
import java.util.UUID
import javax.persistence.Column

class OrderProductDto {
    @JsonProperty("sub_total")
    var subTotal: Double? = null
    var total: Double? = null

    @JsonProperty("total_tax")
    var totalTax: Double? = null

    @JsonProperty("product_uuid")
    var productUuid: UUID? = null
    var quantity: Int? = null
}