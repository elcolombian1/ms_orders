package com.ms.ms_order.dto

import java.util.UUID

open class BaseDto {
    var uuid: UUID? = null
    var tulCode: String? = null
}