package com.ms.ms_order.kafka.factory

interface KafkaProcessorService {

    fun getAction(): KafkaActionOrder
    fun getTopic(): KafkaTopicCatalog
    fun <T> process(request: T)
}
