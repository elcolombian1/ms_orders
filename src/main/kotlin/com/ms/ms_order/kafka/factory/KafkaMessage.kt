package com.ms.ms_order.kafka.factory

open class KafkaMessage {
    var action: String? = null
    var country: String? = null
    var data: Any? = null
}
