package com.ms.ms_order.kafka.factory

import org.springframework.stereotype.Component

@Component
class KafkaProcessorFactory(
    processorServices: Set<KafkaProcessorService>
) {

    private val kafkaProcessorDiscover: MutableMap<Pair<KafkaTopicCatalog, KafkaActionOrder>, KafkaProcessorService> =
        mutableMapOf()

    fun getProcessor(kafkaTopicCatalog: KafkaTopicCatalog, operationName: String?): KafkaProcessorService? {
        return kafkaProcessorDiscover[Pair(kafkaTopicCatalog, KafkaActionOrder.getByName(operationName))]
    }

    init {
        processorServices.forEach { processor ->
            kafkaProcessorDiscover[Pair(processor.getTopic(), processor.getAction())] = processor
        }
    }
}
