package com.ms.ms_order.kafka.factory

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Component

@Component
class KafkaDispatcher(
    private val producer: KafkaTemplate<String, KafkaMessage>,
) : KafkaMessage() {

    private val log = LoggerFactory.getLogger(javaClass)

    fun sendMessage(data: Any, topic: String, action: String) {
        val response = producer.send(topic, buildMessage(data, action))
        log.trace(response.toString())
    }

    private fun buildMessage(data: Any, action: String): KafkaMessage {
        this.data = data
        this.country = "CO"
        this.action = action
        return this
    }
}
