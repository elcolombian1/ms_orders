package com.ms.ms_order.kafka.factory

enum class KafkaActionOrder {
    ORDER_CREATED,
    CONFIRM_DISPATCH,
    CONFIRM_PICKING;

    companion object {

        private val actions = HashMap<String, KafkaActionOrder>()

        fun getByName(name: String?): KafkaActionOrder? {
            return actions.filter {
                it.value.name == name?.uppercase()
            }.values.firstOrNull()
        }

        init {
            values().forEach { country ->
                actions[country.name] = country
            }
        }
    }
}
