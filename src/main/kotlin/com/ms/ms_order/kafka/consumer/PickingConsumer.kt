package com.ms.ms_order.kafka.consumer

import com.ms.ms_order.kafka.factory.KafkaMessage
import com.ms.ms_order.kafka.factory.KafkaProcessorFactory
import com.ms.ms_order.kafka.factory.KafkaTopicCatalog
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component

@Component
class PickingConsumer(
    private val processorFactory: KafkaProcessorFactory
) {
    val log: Logger = LoggerFactory.getLogger(javaClass)

    @KafkaListener(
        topics = ["ms_picking_picking"],
        containerFactory = "config.kafka.consumerFactory"
    )
    fun regularPackageUpdated(message: KafkaMessage) {
        processorFactory.getProcessor(KafkaTopicCatalog.MS_PICKING_PICKING, message.action)
            ?.process(message.data)
    }
}
