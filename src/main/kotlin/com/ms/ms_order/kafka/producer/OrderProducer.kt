package com.ms.ms_order.kafka.producer

import com.ms.ms_order.dto.order.v1.OrderMapper
import com.ms.ms_order.dto.orderProduct.v1.OrderProductMapper
import com.ms.ms_order.kafka.factory.KafkaDispatcher
import com.ms.ms_order.model.Order
import com.ms.ms_order.repository.orderProductRepository.OrderProductRepository
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component

@Component
class OrderProducer(
    private val kafkaDispatcher: KafkaDispatcher,
    private val orderMapper: OrderMapper,
    private val orderProductMapper: OrderProductMapper,
    private val orderProductRepository: OrderProductRepository
) {
    companion object {
        private const val TOPIC: String = "ms_orders_order"
        private const val ACTION: String = "order_created"

    }

    @Async
    fun created(order: Order) {
        val data = orderMapper.modelToDto(order)
        data.orderProducts = orderProductRepository.findByOrderUuid(order.uuid).map(orderProductMapper::modelToDto)
        this.kafkaDispatcher.sendMessage(data = data, topic = TOPIC, action = ACTION)
    }
}