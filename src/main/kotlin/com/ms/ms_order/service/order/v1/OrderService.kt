package com.ms.ms_order.service.order.v1

import com.ms.ms_order.dto.order.v1.OrderDto
import com.ms.ms_order.dto.order.v1.OrderRequest
import com.ms.ms_order.model.Order
import org.springframework.data.jpa.domain.Specification
import java.util.*

interface OrderService {
    fun create(order: OrderRequest): OrderDto
    fun getOne(uuid: UUID): OrderDto
    fun multiple(uuids: List<UUID>): List<OrderDto>
    fun update(uuid: UUID, order: OrderRequest): OrderDto
    fun findAll(spec: Specification<Order>?): List<OrderDto>
}