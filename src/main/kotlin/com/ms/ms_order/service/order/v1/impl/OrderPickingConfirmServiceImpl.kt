package com.ms.ms_order.service.order.v1.impl

import com.ms.ms_order.dto.picking.PickingConfirmDto
import com.ms.ms_order.model.enums.LogisticStatusEnum
import com.ms.ms_order.repository.orderRepository.OrderRepository
import com.ms.ms_order.service.order.v1.OrderPickingConfirmService
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional


@Service
class OrderPickingConfirmServiceImpl(
    val orderRepository: OrderRepository
):OrderPickingConfirmService {
    @Async
    @Transactional
    override fun confirmPickingOnOrder(pickingConfirmDto: PickingConfirmDto) {
        val order= pickingConfirmDto.orderUuid?.let { orderRepository.findById(it) }?.get()
        order?.pickingStatus=LogisticStatusEnum.completed.name
        order?.dispatchStatus=LogisticStatusEnum.pending.name
        order?.let{orderRepository.save(it)}
    }

    @Async
    @Transactional
    override fun confirmDispatchOnOrder(pickingConfirmDto: PickingConfirmDto) {
        val order= pickingConfirmDto.orderUuid?.let { orderRepository.findById(it) }?.get()
        order?.dispatchStatus=LogisticStatusEnum.completed.name
        order?.let{orderRepository.save(it)}
    }
}