package com.ms.ms_order.service.order.v1.impl

import com.ms.ms_order.dto.order.v1.OrderDto
import com.ms.ms_order.dto.order.v1.OrderMapper
import com.ms.ms_order.dto.order.v1.OrderRequest
import com.ms.ms_order.dto.orderProduct.v1.OrderProductMapper
import com.ms.ms_order.kafka.producer.OrderProducer
import com.ms.ms_order.model.Order
import com.ms.ms_order.repository.orderProductRepository.OrderProductRepository
import com.ms.ms_order.repository.orderRepository.OrderRepository
import com.ms.ms_order.service.order.v1.OrderService
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.server.ResponseStatusException
import java.util.*

@Service
class OrderServiceImpl(
    private val orderRepository: OrderRepository,
    private val orderProductRepository: OrderProductRepository,
    private val orderMapper: OrderMapper,
    private val orderProductMapper: OrderProductMapper,
    private val orderProducer: OrderProducer

) : OrderService {

    override fun create(order: OrderRequest): OrderDto {
        val orderSaved = orderRepository.save(orderMapper.requestToModel(order))
        order.orderProducts?.forEach { orderProduct ->
            val orderProductSaved = orderProductMapper.requestToModel(orderProduct)
            orderProductSaved.order = orderSaved
            orderProductRepository.save(orderProductSaved)
        }
        orderProducer.created(orderSaved)
        return orderSaved.let(orderMapper::modelToDto)
    }

    override fun getOne(uuid: UUID): OrderDto {
        return orderMapper.modelToDto(orderRepository.getById(uuid))
    }

    override fun multiple(uuids: List<UUID>): List<OrderDto> {
        return uuids.map {
            try {
                getOne(it)
            } catch (throwable: Throwable) {
                OrderDto()
            }
        }
    }

    @Transactional
    override fun update(uuid: UUID, orderRequest: OrderRequest): OrderDto {
        val order = orderRepository.findById(uuid).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND)
        }
        orderMapper.update(orderRequest, order)
        return orderMapper.modelToDto(orderRepository.save(order))
    }

    override fun findAll(spec: Specification<Order>?): List<OrderDto> {
        return orderRepository.findAll(spec).map(orderMapper::modelToDto)
    }

}