package com.ms.ms_order.service.order.v1

import com.ms.ms_order.dto.order.v1.OrderDto
import com.ms.ms_order.dto.order.v1.OrderRequest
import com.ms.ms_order.dto.picking.PickingConfirmDto
import java.util.*

interface OrderPickingConfirmService {
    fun confirmPickingOnOrder(pickingConfirmDto: PickingConfirmDto)
    fun confirmDispatchOnOrder(pickingConfirmDto: PickingConfirmDto)

}