package com.ms.ms_order.service.kafkaProcessor

import com.fasterxml.jackson.databind.ObjectMapper
import com.ms.ms_order.dto.picking.PickingConfirmDto
import com.ms.ms_order.kafka.factory.KafkaActionOrder
import com.ms.ms_order.kafka.factory.KafkaProcessorService
import com.ms.ms_order.kafka.factory.KafkaTopicCatalog
import com.ms.ms_order.service.order.v1.OrderPickingConfirmService
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service


@Service
class OrderDispatchKafkaConsumer(
    private val objectMapper: ObjectMapper,
    val orderPickingConfirmService: OrderPickingConfirmService
) : KafkaProcessorService {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun getAction() = KafkaActionOrder.CONFIRM_DISPATCH

    override fun getTopic() = KafkaTopicCatalog.MS_PICKING_PICKING

    override fun <T> process(request: T) {
        try {
            process(objectMapper.convertValue(request, PickingConfirmDto::class.java))
        } catch (ex: Exception) {
            logger.error("[process] message = ${ex.message}")
        }
    }

    private fun process(pickingConfirmDto: PickingConfirmDto) {
        orderPickingConfirmService.confirmDispatchOnOrder(pickingConfirmDto)
    }
}
