package com.ms.ms_order.repository.orderRepository

import com.ms.ms_order.model.Order
import com.ms.ms_order.repository.baseRepository.BaseRepository
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface OrderRepository :
    JpaRepository<Order, UUID>,
    JpaSpecificationExecutor<Order>,
    BaseRepository {

}