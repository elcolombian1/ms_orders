package com.ms.ms_order.repository.baseRepository

import com.ms.ms_order.model.base.BaseModel

interface BaseRepository {
    fun findFirstByTulCodeNotNullOrderByCreatedAtDesc(): BaseModel?
}