package com.ms.ms_order.repository.orderProductRepository

import com.ms.ms_order.model.Order
import com.ms.ms_order.model.OrderProduct
import com.ms.ms_order.repository.baseRepository.BaseRepository
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface OrderProductRepository :
    JpaRepository<OrderProduct, UUID>,
    JpaSpecificationExecutor<OrderProduct>,
    BaseRepository {
    fun findByOrderUuid(orderUuid: UUID?): List<OrderProduct>
}