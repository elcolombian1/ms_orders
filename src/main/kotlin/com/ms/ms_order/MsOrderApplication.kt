package com.ms.ms_order

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.web.servlet.config.annotation.EnableWebMvc


@SpringBootApplication
@EnableJpaRepositories
@EnableWebMvc
@EnableAsync
@RefreshScope
class MsOrderApplication

fun main(args: Array<String>) {
    runApplication<MsOrderApplication>(*args)
}
