package com.ms.ms_order.controller.order.v1

import com.ms.ms_order.dto.order.v1.OrderDto
import com.ms.ms_order.dto.order.v1.OrderRequest
import com.ms.ms_order.model.Order
import com.ms.ms_order.service.order.v1.OrderService
import com.sipios.springsearch.anotation.SearchSpec
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.UUID

@RestController
@RequestMapping("v1/orders")
class OrderController(
    private val orderService: OrderService
) {
    @PostMapping
    fun createOrder(@RequestBody order: OrderRequest): ResponseEntity<OrderDto> {
        return ResponseEntity(orderService.create(order), HttpStatus.CREATED)
    }

    @PatchMapping("{orderUuid}")
    fun updateOrder(
        @PathVariable("orderUuid") orderUuid: UUID,
        @RequestBody order: OrderRequest
    ): ResponseEntity<OrderDto> {
        return ResponseEntity(orderService.update(orderUuid, order), HttpStatus.OK)
    }

    @GetMapping("{orderUuid}")
    fun getOrder(@PathVariable orderUuid: UUID): ResponseEntity<OrderDto> {
        return ResponseEntity(orderService.getOne(orderUuid), HttpStatus.OK)
    }

    @PostMapping("/multiple")
    fun getOrderMultiple(@RequestBody orderUuid: List<UUID>): List<OrderDto> {
        return orderService.multiple(orderUuid)
    }

    @GetMapping
    fun getAllOrders(@SearchSpec(caseSensitiveFlag = false) specs: Specification<Order>?,
    ): ResponseEntity<List<OrderDto>> {
        return ResponseEntity(orderService.findAll( Specification.where(specs)), HttpStatus.OK)
    }
}