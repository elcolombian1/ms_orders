import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.springframework.boot") version "2.6.7"
    id("io.spring.dependency-management") version "1.0.11.RELEASE"
    kotlin("jvm") version "1.6.10"
    kotlin("plugin.spring") version "1.6.10"
    kotlin("plugin.jpa") version "1.6.10"
    kotlin("kapt") version "1.4.20"
}

group = "com.ms"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
    mavenCentral()
}

extra["springCloudVersion"] = "2021.0.1"

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    developmentOnly("org.springframework.boot:spring-boot-devtools")
    implementation("com.sipios:spring-search:0.2.4")
    runtimeOnly("org.postgresql:postgresql")
    implementation("com.sipios:spring-search:0.2.4")
    implementation("org.apache.kafka:kafka-clients:3.1.0")
    implementation("org.springframework.kafka:spring-kafka:2.8.5")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    api("org.mapstruct:mapstruct:1.4.2.Final")
    implementation("org.springframework.cloud:spring-cloud-starter-netflix-eureka-client")
    kapt("org.mapstruct:mapstruct-processor:1.4.2.Final")
    implementation("commons-validator:commons-validator:1.7")
    implementation("org.springframework.boot:spring-boot-starter-actuator:2.6.7")
}


dependencyManagement {
    imports {
        mavenBom("org.springframework.cloud:spring-cloud-dependencies:${property("springCloudVersion")}")
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "11"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
